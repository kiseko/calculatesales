package jp.alhinc.koseki_motoyuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {
		//コマンドライン変数の確認
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();

		//支店定義ファイルの読み込み
		if (!fileInput(args[0], "branch.lst", "支店", "^([0-9]{3})$", branchNames,  branchSales)) {
			return;
		}

		//商品定義ファイルの読み込み
		if (!fileInput(args[0], "commodity.lst", "商品", "^([A-za-z0-9]{8})$", commodityNames, commoditySales)) {
			return;
		}

		//売り上げファイルをまとめる
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//売り上げファイル名が連番かの確認
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if ((latter - former) != 1) {
				System.out.println("売り上げファイルが連番になっていません");
				return;
			}
		}

		//売り上げファイルの読み込み
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader saleBr = null;
			try {
				File rcdFile = rcdFiles.get(i);
				FileReader fr = new FileReader(rcdFile);
				saleBr = new BufferedReader(fr);
				List<String> items = new ArrayList<>();
				String line;

				//売り上げファイルを一行ずつリストに追加
				while ((line = saleBr.readLine()) != null) {
					items.add(line);
				}

				if (items.size() != 3) {
					System.out.println(rcdFile.getName() + "のフォーマットが不正です");
					return;
				}

				String branchCode = items.get(0);
				String commodityCode = items.get(1);

				//売上金額が数字かの確認
				if (!items.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long fileSale = Long.parseLong(items.get(2));

				//売り上げファイルの支店コードが支店定義ファイルに存在するかの確認
				if (!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFile.getName() + "の支店コードが不正です");
					return;
				}

				//売り上げファイルの商品コードが商品定義ファイルに存在するかの確認
				if (!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFile.getName() + "の商品コードが不正です");
					return;
				}

				//支店の売上金額の加算
				long branchSaleAmount = branchSales.get(branchCode) + fileSale;

				//商品の売上金額の加算
				long commoditySaleAmount = commoditySales.get(commodityCode) + fileSale;

				//売上金額の合計が10桁以下かの確認
				if (branchSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.replace(branchCode,  branchSaleAmount);
				commoditySales.replace(commodityCode, commoditySaleAmount);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (saleBr != null) {
					try {
						saleBr.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}

		}

		//支店別集計ファイルの出力
		if (!fileOutput(args[0], "branch.out",  branchNames,  branchSales)) {
			return;
		}

		//商品別集計ファイルの出力
		if (!fileOutput(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}


	//入力メソッド
	private static boolean fileInput(String path, String fileName, String fileType, String codeRegex, Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);

			//ファイルの存在確認
			if (!file.exists()) {
				System.out.println(fileType + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");

				//定義ファイルのフォーマット確認
				if ((items.length != 2) || (!items[0].matches(codeRegex))) {
					System.out.println(fileType + "定義ファイルのフォーマットが不正です");
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	//出力メソッド
	private static boolean fileOutput(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key: names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

}
